import React from 'react';
import {
    CustomerServiceOutlined, UserOutlined, LoginOutlined, CalendarOutlined, MailOutlined,
    ShoppingCartOutlined, UserAddOutlined
} from '@ant-design/icons';
import resource from '../assets';
import { Input } from 'antd';
import styled from 'styled-components';


function Header() {
    const HoverDiv = styled.div`
    width: 130px;
    margin-left: 5px; 
    display: flex;
    justify-content: center;
    align-items: center ;
    cursor: pointer;
	&:hover {
    background-color: #f7941e ;
    }
`
    const { Search } = Input;
    const styles = {
        topbar: {
            backgroundColor: '#fff',
            display: 'flex', flexDirection: 'row',
            justifyContent: 'space-around',
            borderColor: 'black', minHeight: 42,
            borderBottom: 1,
            borderBottomColor: 'black',
            marginTop: 10
        },
        listMain: {
            listStyle: 'none',
            marginLeft: 'auto',
            marginRight: 'auto',
        },
        itemTopbar: {
            marginLeft: 8,
            marginRight: 8,
            color: 'black',
            cursor: 'pointer'
        },
        IconNav: {
            fontSize: '25px',
            color: 'black',
            marginLeft: 15,
        },
        divSearch: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
            border: 1,
            borderTopColor: 'black',
            paddingTop: 30
        },
        container: {
            height: 70,
            backgroundColor: '#333333',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            marginTop: 30
        },
        category: {
            width: 250,
            backgroundColor: '#f7941e',
            marginLeft: 5,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        textHome: {
            color: '#fff',
            fontSize: 20,
            fontWeight: 700,
            marginBottom: 0

        }
    };
    const onSearch = value => console.log(value);
    return (
        <header>
            <div className="topbar" style={styles.topbar}>
                <ul className="list-main" style={styles.listMain}>
                    <li style={styles.itemTopbar}> <CustomerServiceOutlined />+060 (800) 801-582</li>
                    <li style={{ ...styles.itemTopbar, marginLeft: 20, cursor: 'pointer' }}><MailOutlined /> support@mtaclothes.com</li>
                </ul>
                <ul className="list-main" style={{ ...styles.listMain, display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                    <li style={styles.itemTopbar}><CalendarOutlined twoToneColor="#52c41a" /> Daily deal</li>
                    <li style={styles.itemTopbar} > <UserOutlined twoToneColor="#52c41a" /> My account</li>
                    <li style={styles.itemTopbar}> <LoginOutlined twoToneColor="#52c41a" /> Login</li>
                </ul>

            </div>
            <div style={{ height: 2, backgroundColor: '#eeeeee' }}></div>
            <div style={styles.divSearch}>
                <img src={resource.images.logo} alt=""></img>
                <Search
                    placeholder="Search product here"
                    allowClear
                    enterButton="Search"
                    size="large"
                    onSearch={onSearch}
                    style={{ width: 600, margin: 0 }}
                />
                <div>
                    <UserAddOutlined style={styles.IconNav} />
                    <ShoppingCartOutlined style={styles.IconNav} />
                </div>

            </div>
            <div style={styles.container}>
                <div style={styles.category}>
                    <p style={styles.textHome}>CATEGORIES</p>
                </div>
                <HoverDiv>
                    <p style={styles.textHome}>Home</p>
                </HoverDiv>
                <HoverDiv>
                    <p style={styles.textHome}>Product</p>
                </HoverDiv>
                <HoverDiv>
                    <p style={styles.textHome}>Service</p>
                </HoverDiv>
                <HoverDiv>
                    <p style={styles.textHome}>Shop</p>
                </HoverDiv>
                <HoverDiv>
                    <p style={styles.textHome}>Blog</p>
                </HoverDiv>
                <HoverDiv>
                    <p style={styles.textHome}>Contact US</p>
                </HoverDiv>

            </div>
        </header>
    );
}

export default Header;