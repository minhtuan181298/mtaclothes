import React from 'react';
import styled, { keyframes } from "styled-components";

function Loader() {
    const bouncedelay = keyframes`
     0% {transform: scale(0, 0); opacity:0.5;}
     100% {transform: scale(1, 1); opacity:0;}
`;

    const Preloader = styled.div`
     position: fixed;
     top: 0;
     left: 0;
     z-index: 999999999;
     width: 100%;
     height: 100%;
     background-color: #fff;
     overflow: hidden;
`;

    const PreloaderInner = styled.div`
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%,-50%);
`;

    const PreloaderIcon = styled.div`
     width: 100px;
     height: 100px;
     display: inline-block;
     padding: 0px;
`;
    const Bounce = styled.div`
     position: absolute;
     display: inline-block;
     width: 100px;
     height: 100px;
     border-radius: 100%;
     background: green;
     animation: ${bouncedelay} 1.6s linear infinite;
     
`;
    const BounceTwo = styled.div`
     position: absolute;
     display: inline-block;
     width: 100px;
     height: 100px;
     border-radius: 100%;
     background:green;
     animation: ${bouncedelay} 1.6s linear infinite;
     animation-delay: -0.8s;
`;

    return (
        <Preloader>
            <PreloaderInner>
                <PreloaderIcon>
                    <Bounce />
                    <BounceTwo />
                </PreloaderIcon>
            </PreloaderInner>
        </Preloader>

    );
}

export default Loader;