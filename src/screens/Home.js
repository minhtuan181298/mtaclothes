import React, { useState, useEffect } from 'react';
import Header from '../components/Header';
import Loader from '../components/Loader';
import ScrollArrow from '../components/ScrollArrow'

function Home() {
    const [spin, setSpin] = useState(true);
    useEffect(() => {
        setTimeout(() => {
            setSpin(false)
        }, 3000)
    })
    return (
        spin ? <Loader /> : <div style={{ height: 3000 }}>
            <Header />

            <ScrollArrow />
        </div>

    );
}

export default Home;